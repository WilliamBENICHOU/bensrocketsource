﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetActiveAtDistance : MonoBehaviour
{

    public float distance = 8f;
    private Transform _target;
    public GameObject group;

    private void Start()
    {
        _target = GameManager.instance.player.GetComponent<Transform>();
    }

    // Update is called once per frame
    public void Update()
    {
        float currDist = Vector3.Distance(transform.position, _target.position);
        bool show = currDist <= distance;
        group.SetActive(show);
    }
}
