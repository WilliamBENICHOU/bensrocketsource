﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundPlayer : MonoBehaviour
{
    public AudioClip[] clips;
    private AudioSource _source;

    private void Awake()
    {
        _source = GetComponent<AudioSource>();
    }

    public void Walk()
    {
        int randId = Random.Range(0, clips.Length);
        _source.PlayOneShot(clips[randId]);
    }
}
