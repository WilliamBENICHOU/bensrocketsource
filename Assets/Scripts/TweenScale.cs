﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TweenScale : MonoBehaviour
{
    public AnimationCurve tweenCurve = AnimationCurve.EaseInOut(0, 0, 1, 1);
    public Vector3 minScale = Vector3.zero, maxScale = Vector3.one;
    private float _currTime = 0;
    public float duration = 1.5f;
    private bool _isPlaying;

    private void Awake()
    {
        _isPlaying = false;
        _currTime = 0;

    }

    private void OnEnable()
    {
        _currTime = 0;
        _isPlaying = true;
        SetValue(0);
    }

    private void SetValue(float t)
    {
        transform.localScale = Vector3.Lerp(minScale, maxScale, t);
    }

    private void OnDisable()
    {
        _isPlaying = false;
        _currTime = 0;
    }


    private void Update()
    {
        if (_isPlaying)
        {
            _currTime += Time.deltaTime;
            if(_currTime >= duration)
            {
                SetValue(tweenCurve.Evaluate(1));
                _isPlaying = false;
            }
            else
            {
                SetValue(tweenCurve.Evaluate(_currTime));
            }
            
        }
    }

}
