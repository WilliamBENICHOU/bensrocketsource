﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(menuName = "Game/Recipe")]
public class RecipeObject : ScriptableObject
{
    [System.Serializable]
    public struct RessourceAmmount
    {
        public RessourceObject res;
        public int ammount;
    }

    public RessourceAmmount[] consumption;
    public RessourceAmmount production;

    public bool IsRessourceNeeded(RessourceObject currRes, List<RessourceObject> ingredientsInside)
    {
        foreach (RecipeObject.RessourceAmmount obj in consumption)
        {
            if (obj.res == currRes && obj.ammount > ingredientsInside.Count(o => o == currRes))
            {
                return true;
            }
        }
        return false;
    }
}
