﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class RecipePresenter : MonoBehaviour
{
    public RawImage icon;
    public TextMeshProUGUI resNameLbl;
    public RecipeElement[] elements;

    public void SetData(RecipeObject data, List<RessourceObject> inside)
    {
        icon.texture = data.production.res.icon;
        resNameLbl.text = data.production.res.resName;
        int i;
        for (i = 0; i < data.consumption.Length && i < elements.Length; i++)
        {

            var cons = data.consumption[i];
            int countInside = inside.Count(o => o == cons.res);
            

            elements[i].gameObject.SetActive(true);
            elements[i].SetData(cons, countInside);
        }
        for (; i < elements.Length; i++)
        {
            elements[i].gameObject.SetActive(false);
        }

    }
}
