﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;

public class GameManager : MonoBehaviour
{
    private bool _canQuit;

    // Start is called before the first frame update
    public static GameManager instance;

    public float gameTime = 240;
    private float leftTime;
    internal GrabbableRes selectedRes;
    internal Refiners SelectedRefiner;
    public Camera cam;
    public PlayerController player;
    public GameObject grabIcon;
    public GameObject dropIcon;
    public RecipePresenter craftWidget;
    public GameObject InterIcon;
    private GameInput _input;
    internal CraftingStation SelectedStation;
    public GameObject CounterIcon;
    public TextMeshProUGUI grabbableNameLbl;
    public Animator fuzeAnimator;
    public GameObject mainCam, endCam;
    public AudioClip endMusic;
    public AudioSource musicSource;
    public AudioSource rocketSource;
    internal int Minutes = 0;
    internal int Seconds = 0;
    
    public GameObject RestartIcon, ui;
    public bool end = false;
    public GameObject endGroup, gameOverGroup;

    void Awake()
    {

        _input = new GameInput();
        _input.Game.Restart.performed += OnRestart;
        SelectedStation = null;
        selectedRes = null;
        SelectedRefiner = null;
        Time.timeScale = 1;
        ui.SetActive(true);
        gameOverGroup.SetActive(false);
        endGroup.SetActive(false);
        _canQuit = false;
        instance = this;
        grabIcon.SetActive(false);
        dropIcon.SetActive(false);
        craftWidget.gameObject.SetActive(false);
        InterIcon.SetActive(false);
        string[] names = Input.GetJoystickNames();
        leftTime = gameTime;

        mainCam.SetActive(true);
        endCam.SetActive(false);

        bool containsJoystick = false;
        foreach(var str in names)
        {
            if (str.Length > 0) containsJoystick = true;
        }

        if (containsJoystick)
        {
            dropIcon.GetComponent<TextMeshProUGUI>().text = "Drop - <color=green>A";
            grabIcon.GetComponent<TextMeshProUGUI>().text = "Grab - <color=green>A";
            InterIcon.GetComponent<TextMeshProUGUI>().text = "Interact - <color=green>A";
        }
    }

    private void OnRestart(InputAction.CallbackContext obj)
    {
        if(_canQuit)
            Application.Quit();
    }

    internal void SetCraftingstation(CraftingStation station)
    {
        SelectedStation = station;
    }



    internal float cameraRotY
    {
        get
        {
            return cam.transform.rotation.eulerAngles.y;
        }
    }
    private int GetLeftMinutes()
    {
        return Mathf.FloorToInt(leftTime / 60f);
    }

    private int GetLeftSeconds()
    {
        return Mathf.FloorToInt(leftTime % 60f);
    }
    private void Update()
    {
        
        leftTime -= Time.deltaTime;
        Minutes = GetLeftMinutes();
        Seconds = GetLeftSeconds();
        CounterIcon.GetComponent<TextMeshProUGUI>().text = Minutes + " : ";
        if (Seconds < 10)
        {
            CounterIcon.GetComponent<TextMeshProUGUI>().text += "0"+GetLeftSeconds();
        }
        else
        {
            
            CounterIcon.GetComponent<TextMeshProUGUI>().text += GetLeftSeconds();
        }
        //Debug.Log(Minutes + Seconds);
        int time = Minutes * 60 + Seconds;
        if (time == 59)
        {
            CounterIcon.GetComponent<TextMeshProUGUI>().color = Color.red;
        }
        if (time <= 0)
        {
            grabIcon.SetActive(false);
            dropIcon.SetActive(false);
            craftWidget.gameObject.SetActive(false);
            InterIcon.SetActive(false);
            CounterIcon.SetActive(false);
            //RestartIcon.SetActive(true);
            end = true;
            ui.SetActive(false);
            _canQuit = true;
            player.gameObject.SetActive(false);
            gameOverGroup.SetActive(true);
        }
        
    }

    internal void SetGrabbable(GrabbableRes grabbableRes)
    {
        if (selectedRes != null)
        {
            selectedRes.Deselect();
        }

        selectedRes = grabbableRes;
        grabbableNameLbl.text = selectedRes.resData.resName;
        selectedRes.Select();
        if (player.CurrRes == null)
        {
            grabIcon.SetActive(true);
        }

    }

    internal void SetRefiner(Refiners obj)
    {
        SelectedRefiner = obj;
        SelectedRefiner.setInteractable(true);
        //Debug.Log(obj.gameObject.name + "selected");
    }
    internal void RemoveRefiner(Refiners obj)
    {
        if(obj == SelectedRefiner)
        {
            SelectedRefiner.setInteractable(false);
            SelectedRefiner = null;
        }
        
        //Debug.Log(obj.gameObject.name + "unselected");
    }

    internal void RemoveCraftingstation(CraftingStation station)
    {
        SelectedStation = null;
    }

  

    internal void Win()
    {
        mainCam.SetActive(false);
        endCam.SetActive(true);
        fuzeAnimator.SetTrigger("end");
        player.gameObject.SetActive(false);
        ui.SetActive(false);
        rocketSource.Play();
        musicSource.clip = endMusic;
        musicSource.Stop();
        musicSource.Play();
        StartCoroutine(ShowEndDelayed());
    }

    private IEnumerator ShowEndDelayed()
    {
        yield return new WaitForSeconds(8);
        endGroup.SetActive(true);
        _canQuit = true;
    }

    internal void RemoveGrabbable(GrabbableRes grabbableRes)
    {
        if (selectedRes == grabbableRes)
        {
            selectedRes.Deselect();
            grabIcon.SetActive(false);
            selectedRes = null;
            grabbableNameLbl.text = "";
        }
    }

    internal void SetUIrecipe()
    {
        //Debug.Log(SelectedRefiner);
        //Debug.Log(SelectedStation);
        string s = "";
        if (craftWidget)
        {
            if (SelectedRefiner != null)
            {
                craftWidget.SetData(SelectedRefiner.recipe, SelectedRefiner.ingredientsInside);
            }
            else if (SelectedStation != null)
            {
                craftWidget.SetData(SelectedStation.CurrPart.recipe, SelectedStation.ingredientsInside);
            }
        }
       
        
    }
}
