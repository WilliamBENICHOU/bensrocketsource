﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CraftingStation : MonoBehaviour
{
    [System.Serializable]
    public class FuzePart
    {
        public GameObject go;
        public RecipeObject recipe;
    }

    public FuzePart[] parts;
    public List<RessourceObject> ingredientsInside;
    public Transform dropPoint;
    public float sendForce;
    public GameObject vfx;
    public FuzePart CurrPart
    {
        get
        {
            if (_currPart >= parts.Length) return null;
            return parts[_currPart];
        }
    }
    public float torque;

    private int _currPart = 0;



    private void Awake()
    {
        ingredientsInside = new List<RessourceObject>();

        Reset();
    }

    private void Reset()
    {
        foreach (FuzePart part in parts)
        {
            part.go.SetActive(false);
        }
    }



    public void Interact()
    {
        var player = GameManager.instance.player;
        var res = player.CurrRes;

        if (CurrPart != null && CurrPart.recipe.IsRessourceNeeded(res.resData, ingredientsInside))
        {
            if (res)
            {
                ingredientsInside.Add(res.resData);
                CheckAdvancement();
            }
        }
        else
        {
            DropObject(res.gameObject);
        }

        res.DestroyRes();
        player.ConsumeRes();
    }

    private void CheckAdvancement()
    {
        bool finished = true;
        foreach (var conso in CurrPart.recipe.consumption)
        {
            if (conso.ammount != ingredientsInside.Count(o => o == conso.res))
            {
                finished = false;
            }
        }

        if (finished)
        {
            CurrPart.go.SetActive(true);
            _currPart++;
            ingredientsInside.Clear();

        }
        if (_currPart >= parts.Length)
        {
            GameManager.instance.Win();
        }
    }

    private void DropObject(GameObject gameObject)
    {
        GameObject instance = Instantiate(gameObject);
        instance.transform.position = dropPoint.position;
        instance.transform.forward = dropPoint.forward;
        Instantiate(vfx, dropPoint.position, Quaternion.identity);
        var rigidbody = instance.GetComponent<Rigidbody>();
        rigidbody.isKinematic = false;
        instance.GetComponent<Collider>().enabled = true;
        rigidbody.AddForce(instance.transform.forward * sendForce, ForceMode.Impulse);
        rigidbody.AddTorque(new Vector3(UnityEngine.Random.value, UnityEngine.Random.value, UnityEngine.Random.value) * torque);
    }



}
