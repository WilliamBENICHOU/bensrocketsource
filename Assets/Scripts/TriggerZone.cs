﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerZone : MonoBehaviour
{
    [SerializeField] private PlayerController _player;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Craftstation"))
        {
            
            CraftingStation obj = other.GetComponent<CraftingStation>();
            GameManager.instance.SetCraftingstation(obj);

            if (_player.CurrRes != null)//resource deja dans les mains
            {
                GameManager.instance.dropIcon.SetActive(false);
                GameManager.instance.InterIcon.SetActive(true);
            }
            GameManager.instance.SetUIrecipe();
            GameManager.instance.craftWidget.gameObject.SetActive(true);
        }
        if (other.CompareTag("Grabbable"))
        {
            GrabbableRes obj = other.GetComponent<GrabbableRes>();
            if (obj.IsGrabbable)
            {
                GameManager.instance.SetGrabbable(obj);
            }

        }
        if (other.CompareTag("Refiner"))
        {
            Refiners obj = other.GetComponent<Refiners>();
            GameManager.instance.SetRefiner(obj);
            GameManager.instance.SetUIrecipe();
            GameManager.instance.craftWidget.gameObject.SetActive(true);
            if (_player.CurrRes != null)//resource deja dans les mains
            {
                GameManager.instance.dropIcon.SetActive(false);
                GameManager.instance.InterIcon.SetActive(true);
            }
        }

    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Craftstation"))
        {
            GameManager.instance.craftWidget.gameObject.SetActive(false);
            CraftingStation obj = other.GetComponent<CraftingStation>();
            GameManager.instance.RemoveCraftingstation(obj);
            GameManager.instance.InterIcon.SetActive(false);
        }
        if (other.CompareTag("Grabbable"))
        {
            GrabbableRes obj = other.GetComponent<GrabbableRes>();
            if (obj.IsGrabbable)
            {
                GameManager.instance.RemoveGrabbable(obj);
            }
        }
        if (other.CompareTag("Refiner"))
        {
            GameManager.instance.craftWidget.gameObject.SetActive(false);
            Refiners obj = other.GetComponent<Refiners>();
            if (_player.CurrRes != null)//resource deja dans les mains
            {
                if (GameManager.instance.InterIcon.activeSelf)
                {
                    GameManager.instance.InterIcon.SetActive(false);
                   
                    
                }
                GameManager.instance.dropIcon.SetActive(true);
            }
            GameManager.instance.RemoveRefiner(obj);
        }
    }
}
