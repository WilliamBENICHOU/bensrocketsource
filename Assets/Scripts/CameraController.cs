﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Transform target;
    public Vector3 offset = new Vector3(3, 4, 3);
    [Range(0,360)]
    public float cameraAngle = 0f;
    private float _currAngle = 0f;
    public float smoothFactorPosition = 0.8f;
    public float smoothFactorCamera = 0.2f;
    private void FixedUpdate()
    {
        _currAngle = Mathf.Lerp(_currAngle, cameraAngle, smoothFactorCamera * Time.fixedDeltaTime);
        Vector3 pos = Vector3.Lerp(transform.position, target.position + offset, smoothFactorPosition * Time.fixedDeltaTime);
        transform.position = pos;
        transform.RotateAround(target.position, Vector3.up, _currAngle);
        transform.LookAt(target);
    }
}
