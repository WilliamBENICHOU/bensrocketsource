﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Biome : MonoBehaviour
{

    public List<GrabbableRes> shitInside;
    public GameObject goodBiome_group, badBiome_group;


    private void Awake()
    {
        shitInside = new List<GrabbableRes>();
        RefreshBiome();
        StartCoroutine(CheckShit());
        
    }

    private IEnumerator CheckShit()
    {
        while (true)
        {
            for (int i = 0; i < shitInside.Count; i++)
            {
                GrabbableRes res = shitInside[i];
                if (res.gameObject == null || res.GetComponent<Rigidbody>().isKinematic)
                {
                    shitInside.RemoveAt(i);
                    RefreshBiome();
                }
                yield return null;
            }
            yield return null;
        }
    }

    private void RefreshBiome()
    {
        bool biomeIsOk = shitInside.Count == 0;
        goodBiome_group.SetActive(biomeIsOk);
        badBiome_group.SetActive(!biomeIsOk);
    }


    private void OnTriggerEnter(Collider other)
    {
        var res = other.GetComponent<GrabbableRes>();
        if(res && res.resData.isBad)
        {
            shitInside.Add(res);
            RefreshBiome();
        }

    }

    private void OnTriggerExit(Collider other)
    {
        var res = other.GetComponent<GrabbableRes>();
        if (res && shitInside.Contains(res))
        {
            shitInside.Remove(res);
            RefreshBiome();
        }
    }
}
