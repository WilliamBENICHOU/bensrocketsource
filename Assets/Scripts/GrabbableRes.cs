﻿using System;
using UnityEngine;

public class GrabbableRes : MonoBehaviour
{
    private Rigidbody _rigidbody;
    private AudioSource _source;
    public RessourceObject resData;
    public float volumeScale = 0.2f;

    public bool IsGrabbable { get; private set; } = true;

    public void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
        _source = GetComponent<AudioSource>();
    }
    internal void Deselect()
    {
        //Debug.Log(gameObject.name + "unselected");
    }

    internal void Select()
    {
        //Debug.Log(gameObject.name + "selected");
    }

    private void OnCollisionEnter(Collision collision)
    {
        float magnitude = collision.relativeVelocity.magnitude;
        if(magnitude > 1)
        {
            _source.volume = Mathf.Clamp01(magnitude / 15) * volumeScale;
            _source.Play();
        }
       
    }

    internal void setGrabbable(bool v)
    {
        _rigidbody.isKinematic = !v;
        IsGrabbable = v;
        GetComponent<Collider>().enabled =  v;
    }

    internal void DestroyRes()
    {
        //spawn particles
        //play audio
        Destroy(gameObject);
    }
}