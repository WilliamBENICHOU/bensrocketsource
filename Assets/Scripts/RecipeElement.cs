﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class RecipeElement : MonoBehaviour
{
    public RawImage icon;
    public TextMeshProUGUI resNameLbl, resAmmountLbl;

    public void SetData(RecipeObject.RessourceAmmount data, int countInside)
    {
        icon.texture = data.res.icon;
        resNameLbl.text = data.res.resName;
        resAmmountLbl.text = countInside + "/" + data.ammount;
    }
}
