﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="Game/Ressource")]
public class RessourceObject : ScriptableObject
{
    public string resName;
    public Texture2D icon;
    public EResources resType;
    public GameObject prefab;
    public bool isBad = true;
}
