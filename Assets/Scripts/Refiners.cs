﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Refiners : MonoBehaviour
{
    public RecipeObject recipe;
    public bool IsInteractable { get; private set; } = true;
    public bool IsWorking { get; private set; }

    public List<RessourceObject> ingredientsInside;
    public Animator animator;
    private AudioSource _audioSource;
    public Transform dropPoint_ok, dropPoint_bad;
    public float sendForce;
    public float torque;
    public GameObject vfx, progressBarGroup;
    public Image progressBar;
    public AudioClip releaseSound;

    private int _currPart = 0;
    public float workDuration = 10f;
    private float _workStartTime;

    internal void setInteractable(bool v)
    {
        IsInteractable = v; 
    }


   



    private void Awake()
    {
        ingredientsInside = new List<RessourceObject>();
        _audioSource = GetComponent<AudioSource>();
        IsWorking = false;
        progressBarGroup.gameObject.SetActive(false);
    }


    private void Update()
    {
        if (IsWorking)
        {
            float fillProgress = (Time.time - _workStartTime) / workDuration;
            progressBar.fillAmount = fillProgress;
        }
    }


    public void Interact()
    {
        if (IsInteractable)
        {
            var player = GameManager.instance.player;
            var res = player.CurrRes;

            if (recipe != null && recipe.IsRessourceNeeded(res.resData, ingredientsInside))
            {
                if (res)
                {
                    ingredientsInside.Add(res.resData);
                    CheckAdvancement();
                }
            }
            else
            {
                DropObject(res.gameObject, dropPoint_bad);
            }

            res.DestroyRes();
            player.ConsumeRes();
        }
       
    }

    private void CheckAdvancement()
    {
        bool finished = true;
        foreach (var conso in recipe.consumption)
        {
            if (conso.ammount != ingredientsInside.Count(o => o == conso.res))
            {
                finished = false;
            }
        }

        if (finished)
        {
            StartCoroutine(Work());
            

        }
    }

    private IEnumerator Work()
    {
        IsInteractable = false;
        IsWorking = true;
        animator.SetBool("working", true);
        _workStartTime = Time.time;
        _audioSource.Play();
        progressBarGroup.gameObject.SetActive(true);
        yield return new WaitForSeconds(workDuration);
        animator.SetBool("working", false);
        IsWorking = false;
        progressBarGroup.gameObject.SetActive(false);
        _audioSource.Stop();
        IsInteractable = true;

        DropObject(recipe.production.res.prefab, dropPoint_ok);
        ingredientsInside.Clear();
    }

    private void DropObject(GameObject gameObject, Transform dropPoint)
    {
        GameObject instance = Instantiate(gameObject);
        instance.transform.position = dropPoint.position;
        instance.transform.forward = dropPoint.forward;

        _audioSource.PlayOneShot(releaseSound);

        Instantiate(vfx, dropPoint.position, Quaternion.identity);

        var rigidbody = instance.GetComponent<Rigidbody>();
        rigidbody.isKinematic = false;
        instance.GetComponent<Collider>().enabled = true;
        rigidbody.AddForce(instance.transform.forward * sendForce, ForceMode.Impulse);
        rigidbody.AddTorque(new Vector3(UnityEngine.Random.value, UnityEngine.Random.value, UnityEngine.Random.value) * torque);
    }



}
