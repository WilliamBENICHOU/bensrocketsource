﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using TMPro;
public class PlayerController : MonoBehaviour
{
    private GameInput _input;
    public float maxSpeed = 200;
    private float _currSpeed = 0;
    private Rigidbody _rigidbody;
    public GrabbableRes CurrRes;
    public Transform grabPoint;
    public Animator animator;
    public float accelerationFactor = 0.5f;
    public float rotationFactor = 0.5f;

    void Awake()
    {
        
        CurrRes = null;
        _input = new GameInput();
        _input.Enable();
        _rigidbody = GetComponent<Rigidbody>();
        RegisterEvent();
    }

    private void RegisterEvent()
    {
        _input.Game.Grab.performed += Grab;
        _input.Game.Restart.performed += Restart;
    }

    private void Restart(InputAction.CallbackContext obj)
    {
        if(GameManager.instance.end)
        {
            SceneManager.LoadScene("WorkScene_will");
        }
    }

    private void Grab(InputAction.CallbackContext obj)
    {
        //obj.action.activeControl.device.deviceId ==1 //keyboard
        //obj.action.activeControl.device.deviceId ==13 //gamepad
        if (CurrRes == null)
        {
            var res = GameManager.instance.selectedRes;
            if (res != null)
            {
                CurrRes = res;
                CurrRes.setGrabbable(false);
                GameManager.instance.RemoveGrabbable(CurrRes);
                CurrRes.transform.parent = grabPoint;
                CurrRes.transform.localPosition = Vector3.zero;
                CurrRes.transform.localRotation = Quaternion.identity;
                if (!GameManager.instance.craftWidget.gameObject.activeSelf) 
                {
                    GameManager.instance.dropIcon.SetActive(true);
                }
            }
        }
        else
        {
            if (GameManager.instance.SelectedStation)
            {
                GameManager.instance.InterIcon.SetActive(false);
                GameManager.instance.SelectedStation.Interact();
                GameManager.instance.SetUIrecipe();
            }
            else if (GameManager.instance.SelectedRefiner)
            {
                GameManager.instance.InterIcon.SetActive(false);
                GameManager.instance.SelectedRefiner.Interact();
                GameManager.instance.SetUIrecipe();
            }
            else
            {
                CurrRes.setGrabbable(true);
                CurrRes.transform.parent = null;
                CurrRes = null;
                GameManager.instance.dropIcon.SetActive(false);
            }
        }
    }

    internal void ConsumeRes()
    {
        CurrRes = null;
    }


    private void Update()
    {
        animator.SetBool("carryBox", CurrRes != null);
        animator.SetBool("moving", _rigidbody.velocity.magnitude > 0.1f);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Vector2 stickValue = _input.Game.Move.ReadValue<Vector2>();
        if (stickValue.magnitude > 0.05f)
        {
            Vector3 direction = new Vector3(stickValue.x, 0, stickValue.y);

            direction = Quaternion.Euler(0, GameManager.instance.cameraRotY, 0) * direction;

            _currSpeed = Mathf.Lerp(_currSpeed, maxSpeed, accelerationFactor);

            _rigidbody.velocity = _currSpeed * Time.fixedDeltaTime * direction + Vector3.up * _rigidbody.velocity.y;
            Vector3 targetView = transform.position + direction;
            Vector3 finalDir = Vector3.Slerp(transform.forward, direction.normalized, rotationFactor);
            _rigidbody.MoveRotation(Quaternion.LookRotation(finalDir, Vector3.up));

        }
        else
        {
            _currSpeed = 0;
        }





    }
}
