﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TweenCanvasGroup : MonoBehaviour
{

    public AnimationCurve tweenCurve = AnimationCurve.EaseInOut(0, 0, 1, 1);
    public float minVal = 0, maxVal = 1;
    private float _currTime = 0;
    public float duration = 1.5f;
    private CanvasGroup _target;
    private bool _isPlaying;

    private void Awake()
    {
        _target = GetComponent<CanvasGroup>();
        _isPlaying = false;
        _currTime = 0;

    }

    private void OnEnable()
    {
        _currTime = 0;
        _isPlaying = true;
        SetValue(0);
    }

    private void SetValue(float t)
    {
        _target.alpha = Mathf.Lerp(minVal, maxVal, t);
    }

    private void OnDisable()
    {
        _isPlaying = false;
        _currTime = 0;
    }


    private void Update()
    {
        if (_isPlaying)
        {
            _currTime += Time.deltaTime;
            if (_currTime >= duration)
            {
                SetValue(tweenCurve.Evaluate(1));
                _isPlaying = false;
            }
            else
            {
                SetValue(tweenCurve.Evaluate(_currTime));
            }

        }


    }

}
