﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAt : MonoBehaviour
{
    public Transform target;
    public Vector3 offsetRot;
    private void Update()
    {
        transform.LookAt(target);
    }
}
